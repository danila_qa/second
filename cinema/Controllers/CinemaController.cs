﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cinema.Controllers
{
    public class CinemaController : Controller
    {
        // GET: Cinema
        public ActionResult Index(string param)
        {
            ViewData["Param"] = param;
            return View();
        }
    }
}